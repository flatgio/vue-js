import Vue from 'vue'
// Components
import './components'
// Plugins
import './plugins'
import ability from './plugins/ability'
import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'

Vue.use(VueChartkick, {adapter: Chart})
// Sync router with store
import { sync } from 'vuex-router-sync'

// Application imports
import App from './App'
import i18n from '@/i18n'
import router from '@/router'
import store from '@/store'

const token = localStorage.getItem('token')

if (token) {
    Vue.prototype.$http.defaults.headers.common['Authorization'] = token
}

// Sync store with router
sync(store, router)

Vue.config.productionTip = false


/* eslint-disable no-new */
new Vue({
  i18n,
  router,
  store,
  ability,
  render: h => h(App)
}).$mount('#app')
