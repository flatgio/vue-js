// https://vuex.vuejs.org/en/mutations.html

export default {

    SET_ORDERS: function(state, payload) {
        state.orders = payload.data
    },

    SET_ITEMS: function(state, payload) {
        state.items = payload.data.entries
    },

    SET_STATS: function(state, payload) {
        state.stats = payload.data
    },

    SET_REFS: function(state, payload) {
        state.refs = payload.data
    },

    SET_ITEM_LIST: function(state, payload) {
        state.itemList = payload
    },

    REMOVE_ITEM_FROM_LIST: function (state, payload) {
        state.items.splice(state.items.indexOf(payload), 1)
    },

    REMOVE_ITEM: function (state, payload) {
        state.itemList.splice(state.itemList.indexOf(payload), 1)
    },
    
    AUTH_SUCCESS: function (state, token) {

        stete.token = token
    }
}
