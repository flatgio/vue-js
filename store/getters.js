// https://vuex.vuejs.org/en/getters.html

export default {
    isLoggedIn: state => !!state.token,

    orders: (state) => {
        return state.orders
    },

    orderPayment: (state) => {
        return state.orders.payment
    },

    orderShipped: (state) => {
        return state.orders.shipped
    },

    orderShippedNow: (state) => {
        return state.orders.now_shipped
    },

    orderWarehouse: (state) => {
        return state.orders.warehouse
    },

    orderTime: (state) => {
        return state.orders.time
    },

    orderWithoutPrepayment: (state) => {
        return state.orders.without_prepayment
    },

    orderPrepayment: (state) => {
        return state.orders.prepayment
    },

    orderCredit: (state) => {
        return state.orders.credit
    },

    orderExpress: (state) => {
        return state.orders.express
    },

    getItems: (state) => {
        return state.items
    },

    getItemList: (state) => {
        return state.itemList
    },

    getStats: (state) => {
        return state.stats
    },

    getRefs: (state) => {
        return state.refs
    }
}
