// https://vuex.vuejs.org/en/state.html

export default {
    token: localStorage.getItem('token') || '',
    orders: [],
    is_loading: false,
    items: [],
    itemList: [],
    stats: [],
    refs: [],
}
