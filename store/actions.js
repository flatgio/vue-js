import axios from 'axios'
import apiHost from '../config'

export default {
    login: function ({commit}, user){
        return new Promise((resolve, reject) => {
            commit('AUTH_REQUEST')
            axios.post(apiHost +'/users/login', user)
                .then(resp => {
                    const token = resp.data.token
                    const user = resp.data.user
                    localStorage.setItem('token', token)
                    axios.defaults.headers.common['Authorization'] = token
                    commit('AUTH_SUCCESS', token, user)
                    resolve(resp)

                    window.location.href = '/backend'
                })
                .catch(err => {
                    commit('AUTH_ERROR')
                    localStorage.removeItem('token')
                    reject(err)
                })
        })
    },

    loadOrders: async function ({ commit }) {
       await axios.get(apiHost + '/orders').then((response) => {
            commit('SET_ORDERS', response)
        })
    },

    loadItemByName: function ({ commit }, name) {
        axios.get(apiHost + '/items/search/'+ name).then((response) => {
            commit('SET_ITEMS', response)
        })
    },

    loadItemList: function ({ commit }) {
        axios.get(apiHost + '/items/list').then((response) => {
            commit('SET_ITEM_LIST', response.data.data)
        })
    },

    saveItem: function ({ commit }, id) {
        axios.post(apiHost + '/items', { id: id }).then((response) => {
        })
    },

    removeItemFromList: function ({ commit }, item) {
            commit('REMOVE_ITEM_FROM_LIST', item)
    },

    removeItem: function ({ commit }, item) {
        axios.get(apiHost + '/items/remove/'+ item.id).then((response) => {
            commit('REMOVE_ITEM', item)
        })
    },

    loadStats: async function ({ commit }) {
      await axios.get(apiHost + '/stats').then((response) => {
        commit('SET_STATS', response)
      })
    },

    loadRefs: async function ({ commit }) {
      await axios.get(apiHost + '/stats/refs').then((response) => {
        commit('SET_REFS', response)
      })
    },

    setActiveReferrer: function ({ commit }, data) {
      axios.post(apiHost + '/stats/active/' + data.id, {is_active:data.isactive});

    }

}
