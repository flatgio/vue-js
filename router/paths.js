export default [

  {
    path: '/login',
    name: 'Login',
    view: 'Login'
  },

  {
    path: '/dashboard',
    // Relative to /src/views
    view: 'Dashboard',
      meta: {
          requiresAuth: true
    }
  },
  {
    path: '/articles',
    name: 'Artikel',
    view: 'ArticleList'
  },
  {
    path: '/statistics',
    name: 'Statistiken',
    view: 'Statistic'
  },

  {
    path: '/statistics-settings',
    name: 'Statistik-Einstellungen',
    view: 'StatisticSetting'
  }
]
